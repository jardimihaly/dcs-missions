---- Returns all zones created in the ME
-- @return #table Table of zones
function getZones()
  local prefix = 'AREA:'
  local difficulties = { 'EASY', 'MEDIUM', 'HARD', 'VERYHARD' }
  
  local zones = {  }
  
  for i,difficulty in pairs(difficulties) do
    env.info("DIFFICULTY:" .. difficulty)
    local zoneGroupPrefix = prefix .. difficulty
    local set = SET_ZONE:New():FilterPrefixes(zoneGroupPrefix):FilterStart()
    zones[difficulty] = set
  end
  
  return zones
end

randomTemplate = SPAWN:New("Target Group"):InitRandomizeTemplatePrefixes("TEMPLATE:"):InitSkill("Random"):InitCleanUp(3600)
samTemplate = SPAWN:New("TEMPLATE:SAM"):InitSkill("Random"):InitCleanUp(3600)
zones = getZones()

do
  function spawnRandomTarget(difficulty)
    env.info("SPAWN " .. difficulty)
    local zone = zones[difficulty]:GetRandomZone()
    local vector2 = zone:GetRandomPointVec2()
    
    local group = randomTemplate:SpawnFromPointVec2(vector2)
    
    local count = group:GetDCSObject():getInitialSize()
    local name = nil
    if(count == 7 or count == 9 or count == 6) then
      name = "Insurgents"
    elseif(count == 11) then
      name = "Military Outpost"
    else
      name = "SAM Site"
    end
    
    local coords = zone:GetCoordinate()
    coords:MarkToCoalitionBlue(name)
    
    MESSAGE:New("Enemies detected at [" .. coords:ToStringLLDMS() .. "]. Check your map for more information.", 300):ToBlue()
  end
  
  function spawnSAM()
    env.info("SPAWN SAM")
    
    local zone = zones["VERYHARD"]:GetRandomZone()
    local vector2 = zone:GetRandomPointVec2()
    
    local group = samTemplate:SpawnFromPointVec2(vector2)
    
    local coords = zone:GetCoordinate()
    
    coords:MarkToCoalitionBlue("SAM Site")
    MESSAGE:New("Enemies detected at [" .. coords:ToStringLLDMS() .. "]. Check your map for more information.", 300):ToBlue()
  end

  local spawnMenu = MENU_COALITION:New(coalition.side.BLUE,"Spawn")
  MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Easy Targets",spawnMenu,spawnRandomTarget,"EASY")
  MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Medium Targets",spawnMenu,spawnRandomTarget,"MEDIUM")
  MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Hard Targets",spawnMenu,spawnRandomTarget,"HARD")
  MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Very Hard Targets",spawnMenu,spawnRandomTarget,"VERYHARD")
  MENU_COALITION_COMMAND:New(coalition.side.BLUE,"SAM Site",spawnMenu,spawnSAM)
end

