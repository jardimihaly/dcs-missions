# DCS Missions

First of all, why not use DCS user files? The answer is simple: I wanted a 
centralized repository for all the missions created by me.


Please note, that these missions are designed for small groups of people,
and won't be as high-quality as most of the missions in the DCS user files 
site.

If you find bugs, spelling errors, etc., feel free to message me and I'll try to
make corrections when I have the time.

**Note: I won't add missions not created by me.**

## Layout
The missions are currently being made for 2 maps: Caucasus and Persian Gulf.
In their respective folders, you can find subfolders for different mission 
types.

---
**WORK IN PROGRESS**